/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
// const { pathsToModuleNameMapper } = require('ts-jest');
// const {
//   compilerOptions: { paths: tsconfigPaths },
// } = require('./tsconfig');

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    './src/utils/jest.setup.ts',
  ],
  roots: ['<rootDir>/src'],
  modulePaths: ['<rootDir>/src'],
  moduleNameMapper:
    // ...pathsToModuleNameMapper(tsconfigPaths),

    {
      '^@/(.*)': '<rootDir>/src/$1',
      '\\.(css|less|scss|sass)$': '<rootDir>/node_modules/identity-obj-proxy',
      // '\\.(jpg|jpeg|png)$': '<rootDir>/node_modules/identity-obj-proxy',
      '.+\\.(png|jpg|jpeg|svg)$': 'jest-transform-stub',
    },

  // moduleDirectories: ['node_modules', '<rootDir>'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  verbose: true,
  clearMocks: true,
};
