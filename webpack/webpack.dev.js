const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  devServer: {
    port: 3000,
    hot: true, //* hot reloading
    historyApiFallback: true, //* refresh 404
  },
  devtool: 'cheap-module-source-map',

  plugins: [new MiniCssExtractPlugin(), new ReactRefreshPlugin()],
};
