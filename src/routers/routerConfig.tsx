import { AppURL } from '@/constants';
import React from 'react';
const Home = React.lazy(
  () => import(/* webpackChunkName: 'Home' */ 'pages/Home/Home')
);

const Product = React.lazy(
  () => import(/* webpackChunkName: 'Product' */ 'pages/Product/Product')
);
const ProductUpdate = React.lazy(
  () =>
    import(
      /* webpackChunkName: 'ProductUpdate' */ 'pages/Product/ProductUpdate'
    )
);

type RoutesProps = {
  url: string;
  title: string;
  component?: React.ReactElement;
  hideNavigate?: boolean;
};

export const appRouters: RoutesProps[] = [
  {
    url: AppURL.HOME,
    title: 'Home',
    component: <Home />,
  },
  {
    url: AppURL.PRODUCT,
    title: 'Product Info',
    component: <Product />,
  },
  {
    url: AppURL.PRODUCT_EDIT,
    title: 'Product Edit',
    component: <ProductUpdate />,
    hideNavigate: true,
  },
  {
    url: AppURL.PRODUCT_CREATE,
    title: 'Product Create',
    component: <ProductUpdate />,
    hideNavigate: true,
  },
];
