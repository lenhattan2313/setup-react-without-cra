import React from 'react';
import Header from '../Header/Header';

interface Props {
  children?: React.ReactNode;
}
const Main: React.FC<Props> = ({ children }) => {
  return (
    <>
      <Header />
      <div style={{ height: 60 }}></div>
      <main> {children}</main>
    </>
  );
};
export default Main;
