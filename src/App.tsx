import React from 'react';
import { useSelector } from 'react-redux';
import { ModalContainer, ToastContainer } from './components';
import { ToastProvider } from './context';
import { RootState } from './redux/reducers';
import Router from './routers';
const App = () => {
  return (
    <ToastProvider>
      <Router />
      <ToastContainer />
      <ModalContainer />
    </ToastProvider>
  );
};
export default App;
