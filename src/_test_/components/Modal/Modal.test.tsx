import { Modal } from '@/components';
import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';

describe('Modal component', () => {
  it('should render the component onto the screen', () => {
    const { getByText } = render(
      <Modal title="Modal title" footer={[<span>footer button</span>]}>
        <h1>text body</h1>
      </Modal>
    );
    expect(screen.getByTestId('title')).toHaveTextContent('Modal title');
    expect(screen.getByTestId('footer').childNodes.length).toBe(1);
    expect(getByText(/text body/i)).toBeInTheDocument();
  });
  it('should close the component', () => {
    render(
      <Modal title="Modal title">
        <h1>text body</h1>
      </Modal>
    );
    const buttonElement = screen.getByTestId('button');

    fireEvent.click(buttonElement);
    expect(document.getElementsByName('modal-dialog').length).toBe(0);
  });
});
