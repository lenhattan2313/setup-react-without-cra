import { ToastAction } from '@/constants';
import { IToastInfo } from '@/types';
import React, { useContext, useReducer } from 'react';

const initialToast: ToastState = {
  toastList: [],
};
export const ToastContext = React.createContext<{
  state: ToastState;
  dispatch: React.Dispatch<any>;
}>({
  state: initialToast,
  dispatch: () => null,
});

interface ActionPayload {
  type: string;
  payload: IToastInfo;
}
interface ToastState {
  toastList: IToastInfo[];
}

export function ToastReducer(state = initialToast, action: ActionPayload) {
  switch (action.type) {
    case ToastAction.ADD:
      return {
        ...state,
        toastList: [...state.toastList, action.payload],
      };
    case ToastAction.DELETE:
      return {
        ...state,
        toastList: state.toastList.filter(
          (toast) => toast.id !== action.payload.id
        ),
      };
    default:
      return state;
  }
}

export const ToastProvider = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(ToastReducer, initialToast);
  return (
    <ToastContext.Provider value={{ state, dispatch }}>
      {children}
    </ToastContext.Provider>
  );
};

export const useToastContext = () => useContext(ToastContext);
