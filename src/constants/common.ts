export const AppURL = {
  HOME: '/',
  PRODUCT: '/product',
  PRODUCT_EDIT: '/product/:id',
  PRODUCT_CREATE: '/product-create',
  NOTFOUND: '/notfound',
};
export const Config = {
  BASE_API: 'https://dummyjson.com',
};
export const AxiosMethod = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE',
};
export enum ToastType {
  SUCCESS = 'success',
  ERROR = 'error',
  WARNING = 'warning',
}
export enum ToastAction {
  ADD = 'add',
  DELETE = 'delete',
}
