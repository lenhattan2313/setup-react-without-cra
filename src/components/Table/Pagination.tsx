import React from 'react';

interface PaginationProps {
  limitPerPage: number;
  totalList: number;
  onPageChange: (pagination: number) => void;
  currentPage: number;
}
const Pagination: React.FC<PaginationProps> = ({
  limitPerPage,
  totalList,
  onPageChange,
  currentPage,
}) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalList / limitPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <>
      <nav aria-label="Page navigation example" data-testid="pagination">
        <ul className="pagination justify-content-end m-0">
          {pageNumbers.map((pagination) => (
            <li
              className={`${
                currentPage === pagination ? 'active' : ''
              } page-item`}
              key={pagination}
            >
              <button
                className={`${
                  currentPage === pagination ? 'active' : ''
                } page-link`}
                onClick={() => onPageChange(pagination)}
                data-testid="page_link"
              >
                {pagination}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default Pagination;
