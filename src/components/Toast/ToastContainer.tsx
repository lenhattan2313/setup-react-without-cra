import { useToastContext } from '@/context';
import React from 'react';
import ReactPortal from '../Portal/ReactPortal';
import Toast from './Toast';

const ToastContainer = () => {
  const {
    state: { toastList },
  } = useToastContext();

  return (
    <ReactPortal wrapperId="react-portal-toast-container">
      <div
        className="position-absolute end-0 me-3 mt-2 top-0"
        style={{ zIndex: 10000 }}
        data-testid="toast-container"
      >
        {toastList &&
          toastList.map((toast) => (
            <Toast
              id={toast.id}
              key={toast.id}
              type={toast.type}
              message={toast.message}
            />
          ))}
      </div>
    </ReactPortal>
  );
};

export default ToastContainer;
